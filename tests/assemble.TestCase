#! /usr/bin/env python3
#
# Copyright (C) 2017, Michael Poehn <michael.poehn@fsfe.org>


import os
import sys
import json
import base64
import codecs
import shutil
import inspect
import zipfile
import tempfile
import unittest
import unittest.mock

localmodule = os.path.realpath(
    os.path.join(os.path.dirname(
        inspect.getfile(inspect.currentframe())), '..'))
if localmodule not in sys.path:
    sys.path.insert(0, localmodule)

from helper import TmpCwd
import provilib.assemble


class ProvisionTest(unittest.TestCase):

    def test_run_plain(self):
        with tempfile.TemporaryDirectory() as tmpdir:
            tmpcreddir = os.path.join(tmpdir, 'credentials')
            os.mkdir(tmpcreddir)
            shutil.copy('../examples/demo_credentials.yaml', tmpcreddir)

            with TmpCwd(tmpdir):
                p = provilib.assemble.Assemble()
                options = unittest.mock.Mock()
                options.verbose = True
                options.no_obfuscation = True
                p.run(options)

                self.assertTrue(os.path.isdir('provisions'))
                self.assertTrue(os.path.isfile(
                    'provisions/demo_credentials_user1.fdrp'))
                self.assertTrue(os.path.isfile(
                    'provisions/demo_credentials_user2.fdrp'))

                with zipfile.ZipFile(
                        'provisions/demo_credentials_user1.fdrp', 'r') as f:

                    d = json.loads(str(f.read('repo_provision.json'), 'utf8'))
                    expectedsig = "11112222333344445555666677778888" \
                                  "99990000aaaabbbbccccddddeeeeffff"
                    self.assertDictEqual({"name": "Example Repo",
                                          "password": "secret1",
                                          "url": "https://example.com"
                                                 "/fdroid/repo",
                                          "sigfp": expectedsig,
                                          "username": "user1"}, d)

    def test_run_obfuscated(self):
        with tempfile.TemporaryDirectory() as tmpdir:
            tmpcreddir = os.path.join(tmpdir, 'credentials')
            os.mkdir(tmpcreddir)
            shutil.copy('../examples/demo_credentials.yaml', tmpcreddir)

            with TmpCwd(tmpdir):
                p = provilib.assemble.Assemble()
                options = unittest.mock.Mock()
                options.verbose = True
                options.no_obfuscation = False
                p.run(options)

                self.assertTrue(os.path.isdir('provisions'))
                self.assertTrue(os.path.isfile(
                    'provisions/demo_credentials_user1.fdrp'))
                self.assertTrue(os.path.isfile(
                    'provisions/demo_credentials_user2.fdrp'))

                with zipfile.ZipFile(
                        'provisions/demo_credentials_user2.fdrp', 'r') as f:
                    obf = str(f.read('repo_provision.ojson'), 'utf8')
                    d = json.loads(str(base64.b64decode(
                        codecs.decode(obf, 'rot_13')), 'utf8'))
                    expectedsig = "11112222333344445555666677778888" \
                                  "99990000aaaabbbbccccddddeeeeffff"
                    self.assertDictEqual({"name": "Example Repo",
                                          "password": "other secret",
                                          "sigfp": expectedsig,
                                          "url": "https://example.com"
                                                 "/fdroid/repo",
                                          "username": "user2"}, d)


if __name__ == "__main__":
    os.chdir(os.path.dirname(__file__))

    newSuite = unittest.TestSuite()
    newSuite.addTest(unittest.makeSuite(ProvisionTest))
    unittest.main(failfast=False)
