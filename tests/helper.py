#!/usr/bin/env python3

import os


class TmpCwd():
    """Context-manager for temporarily changing the current working
    directory.
    """

    def __init__(self, new_cwd):
        self.new_cwd = new_cwd

    def __enter__(self):
        self.orig_cwd = os.getcwd()
        os.chdir(self.new_cwd)

    def __exit__(self, a, b, c):
        os.chdir(self.orig_cwd)
