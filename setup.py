#! /usr/bin/env python3

from setuptools import setup

setup(name='provi',
      version='0.3',
      description='Provisioning tool for F-Droid',
      author='Michael Poehn',
      author_email='michael.poehn@fsfe.org',
      url='https://gitlab.com/uniqx/provi',
      license='AGPL-3.0',
      packages=[
          'provilib',
      ],
      scripts=['provi'],
      python_requires='>=3.4',
      install_requires=[
          'PyYAML',
      ])
