#! /usr/bin/env python3
#
# Copyright (C) 2017, Michael Poehn <michael.poehn@fsfe.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import logging


class CmdPlugin():
    def __init__(self, subcommand):
        self.subcommand = subcommand

    def run(self, options):
        logging.debug("run '{}'".format(self.subcommand))

    def setup_subparser(self, subparsers):
        p = subparsers.add_parser(self.subcommand, help=self.get_help())
        self.add_subparser_args(p)

    def add_subparser_args(self, subparser):
        logging.debug("add_subparser_args for '{}'".format(self.subcommand))

    def get_help(self):
        return None
