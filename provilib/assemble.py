#! /usr/bin/env python3
#
# Copyright (C) 2017, Michael Poehn <michael.poehn@fsfe.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import os
import json
import yaml
import codecs
import base64
import logging
import zipfile

from . import common
from . import plugin
from . import exception


class Assemble(plugin.CmdPlugin):

    def __init__(self):
        super(Assemble, self).__init__('assemble')

    def get_help(self):
        return "create provisioning files for fdroid-client app."

    def add_subparser_args(self, subparser):
        subparser.add_argument('-c', '--continue-on-failure',
                               action='store_true', default=False,
                               help="try continue to write all provisioning "
                               "files regardless of errors")
        subparser.add_argument('-n', '--no-obfuscation',
                               action='store_true', default=False,
                               help="store provisions as plain text "
                               "instead of obfuscating them")

    def run(self, options):
        logging.debug('running {}'.format(self.subcommand))

        credentials_dir = common.get_local_dir('credentials')
        provisions_dir = common.create_local_dir('provisions')

        total_written_provisions = 0
        for dirpath, dirnames, filenames in os.walk(credentials_dir):
            for filename in filenames:
                filepath = os.path.join(dirpath, filename)
                repocredentials = {}
                if filename.endswith('.yaml') or filename.endswith('.yml'):
                    with open(filepath, 'r') as f:
                        repocredentials = yaml.load(f.read())
                elif filename.endswith('.json'):
                    with open(filepath, 'r') as f:
                        repocredentials = json.load(f)

                if repocredentials:
                    logging.info('processing {}'.format(filepath))
                    exceptions = []
                    try:
                        common.assert_repocredentials(
                            {'path': filepath, 'content': repocredentials})
                        prefix = os.path.splitext(filename)[0]
                        c = self.write_provisions(prefix, repocredentials,
                                                  provisions_dir,
                                                  not options.no_obfuscation)
                        total_written_provisions += c
                    except exception.ProviException as e:
                        exceptions.append(e)
                        if not options.continue_on_failure:
                            raise e
                    if exceptions:
                        for e in exceptions:
                            logging.warn(e)
        logging.info('assembled {} provisions to \'./provisions\''
                     .format(total_written_provisions))

    def obfuscate(self, text):
        return codecs.encode(base64.b64encode(
            text.encode('utf8')).decode('ascii'), 'rot_13')

    def write_provisions(self, prefix, repocredentials,
                         provisions_dir, obfuscate=True):
        for user in repocredentials['users']:
            text = json.dumps({'name': repocredentials['repo']['name'],
                               'url': repocredentials['repo']['url'],
                               'sigfp': repocredentials['repo']['sigfp'],
                               'username': user['username'],
                               'password': user['password']})
            provision_basepath = os.path.join(
                provisions_dir, prefix + '_' + user['username'])
            with zipfile.ZipFile(provision_basepath + '.fdrp', 'w') as f:
                if obfuscate:
                    info = zipfile.ZipInfo('repo_provision.ojson')
                    info.compress_type = zipfile.ZIP_DEFLATED
                    info.create_system = 0
                    f.writestr(info, self.obfuscate(text))
                else:
                    info = zipfile.ZipInfo('repo_provision.json')
                    info.compress_type = zipfile.ZIP_DEFLATED
                    info.create_system = 0
                    f.writestr(info, text)
        return len(repocredentials['users'])
