#! /usr/bin/env python3
#
# Copyright (C) 2017, Michael Poehn <michael.poehn@fsfe.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import os
import shlex
import shutil
import logging
import subprocess

from . import common
from . import plugin
from . import exception


class Provision(plugin.CmdPlugin):

    def __init__(self):
        super(Provision, self).__init__('provision')

    def get_help(self):
        return "deploy individual provision file " \
               "to an Android device via adb"

    def add_subparser_args(self, subparser):
        subparser.add_argument('PROVISION_FILE', nargs='*',
                               default=None,
                               help="provisioning file (.fdrp)")
        subparser.add_argument('--app-package',
                               default="org.fdroid.fdroid",
                               help="alternative app package name. "
                                    "(eg. for debug or rebranded "
                                    "builds of F-Droid client app.)")

    def run(self, options):
        logging.debug('running {}'.format(self.subcommand))

        if not shutil.which('adb'):
            raise exception.ProviException("adb is not installed.")

        if not options.PROVISION_FILE:
            raise exception.ProviException('Please supply a .fdrp file for '
                                           'provisioning it to an Android '
                                           'device. (see: -h for help)')

        repo_dir = common.create_local_dir('repo')

        for provision_file in options.PROVISION_FILE:
            path = provision_file if provision_file.endswith('.fdrp') \
                else provision_file + '.fdrp'
            if not os.path.isfile(path):
                path = os.path.join(repo_dir, path)
                if not os.path.isfile(path):
                    raise exception.ProviException(
                        "could not find provision: '{}'"
                        .format(provision_file))

            logging.info("provisioning '{}' via adb ...".format(path))

            provisions_dir = os.path.join('/', 'storage', 'emulated', '0',
                                          'Android', 'data',
                                          options.app_package,
                                          'files', 'provisions')
            ret = subprocess.call(['adb', 'shell', 'mkdir',
                                   '-p', shlex.quote(provisions_dir)])
            if ret > 0:
                raise exception.ProviException('adb command failed: '
                                               'please make sure only '
                                               'one device is connected '
                                               'and ready to use.')
            subprocess.call(['adb', 'push', path, provisions_dir])

        logging.info('Pushed {} provison(s) to device.'
                     .format(len(options.PROVISION_FILE)))

        subprocess.call(['adb', 'shell', 'am', 'force-stop',
                         shlex.quote(options.app_package)])
        subprocess.call(['adb', 'shell', 'monkey',
                         '-p', shlex.quote(options.app_package),
                         '-c', 'android.intent.category.LAUNCHER', '1'])
