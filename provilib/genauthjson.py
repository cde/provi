#! /usr/bin/env python3
#
# Copyright (C) 2017, Michael Poehn <michael.poehn@fsfe.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import os
import json
import hashlib
import logging

from . import common
from . import plugin


class GenAuthJson(plugin.CmdPlugin):

    def __init__(self):
        super(GenAuthJson, self).__init__('genauthjson')

    def get_help(self):
        return "generate json file containing http auth credentials"

    def add_subparser_args(self, subparser):
        pass

    def run(self, options):
        logging.debug('running {}'.format(self.subcommand))

        credentials_dir = common.get_local_dir('credentials')
        repo_dir = common.create_local_dir('repo')

        credentials_list = common.find_and_parse_credentials(credentials_dir)

        for credentials in credentials_list:

            common.assert_repocredentials(credentials)
            prefix = common.credential_name(credentials)
            salt = hashlib.sha256('f-droid.org'.encode('utf-8')).hexdigest()

            auths = []
            for user in credentials.get('content').get('users'):
                p = hashlib.sha256(
                    (salt + user['password']).encode('utf-8')).hexdigest()
                auths.append([user['username'], p])

            dist_path = os.path.join(repo_dir,
                                     prefix + '.http-credentials.json')
            with open(os.path.join(dist_path), 'w') as f:
                json.dump(auths, f)
            logging.info("generated '{}'".format(dist_path))
