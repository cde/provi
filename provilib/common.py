#! /usr/bin/env python3
#
# Copyright (C) 2018, Michael Poehn <michael.poehn@fsfe.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import os
import json
import yaml
import logging

from . import exception


def get_local_dir(dir_name, root_dir=None):
    """Checks if a directory is present and raises exeception if not"""

    path = None
    if root_dir:
        path = os.path.join(os.path.abspath(root_dir), dir_name)
    else:
        path = os.path.join(os.getcwd(), dir_name)

    if not os.path.isdir(path):
        if os.path.exists(path):
            raise exception.ProviException(
                "Can not find {dir_name} directory '{dir_path}',"
                "a file with that name already exists."
                .format(dir_name=dir_name, dir_path=path))
        else:
            raise exception.ProviException(
                "Could not find {dir_name} directory '{dir_path}'"
                .format(dir_name=dir_name, dir_path=path))
    return path


def create_local_dir(dir_name, root_dir=None, no_log=False):

    path = None
    if root_dir:
        path = os.path.join(os.path.abspath(root_dir), dir_name)
    else:
        path = os.path.join(os.getcwd(), dir_name)

    if not os.path.exists(path):
        if not no_log:
            logging.info('creating {dir_name} directory: {dir_path}'
                         .format(dir_name=dir_name, dir_path=path))
        os.makedirs(path)
    elif not os.path.isdir(path):
        raise exception.ProviException(
            "Can not create {dir_name} directory '{dir_path}',"
            "a file with that name already exists."
            .format(dir_name=dir_name, dir_path=path))
    return path


def find_and_parse_credentials(credentails_dir):
    """Searches for yaml/json files inside a direcotry and parses
       them under the assumption that the contain credentials."""
    result = []
    for dirpath, dirnames, filenames in os.walk(credentails_dir):
        for filename in filenames:
            filepath = os.path.join(dirpath, filename)
            try:
                if filename.endswith('.yaml') or filename.endswith('.yml'):
                    with open(filepath, 'r') as f:
                        result.append({'path': filepath,
                                       'content': yaml.load(f.read())})
                elif filename.endswith('.json'):
                    with open(filepath, 'r') as f:
                        result.append({'path': filepath,
                                       'content': json.load(f)})
            except (yaml.parser.ParserError, ValueError) as e:
                raise exception.ProviException("Could not parse '{}'"
                                               .format(filepath)) from e
    return result


def assert_repocredentials(credentials):
    # assert data structure
    if not credentials.get('path'):
        raise exception.ProviException(
            "credentials data structure corrupted: no 'path' "
            "item present.")
    if not credentials.get('content'):
        raise exception.ProviException(
            "credentials data structure corruped, no 'content' "
            "item present.")

    # assert credential content
    def assert_content_path(datapath, filepath):
        pts = datapath.split('.')
        cur = credentials['content']
        curpath = ''
        for pt in pts:
            curpath += '.' + pt if curpath else pt
            if type(cur) == dict and not cur.get(pt):
                raise exception.ProviException(
                    "'{filepath}' malformed, "
                    "should contain '{jsonpath}' section. "
                    "(see examples/demo_credentials.yaml)"
                    .format(filepath=filepath, jsonpath=curpath))
            cur = cur.get(pt)

    assert_content_path('repo.name', credentials['path'])
    assert_content_path('repo.url', credentials['path'])
    assert_content_path('repo.sigfp', credentials['path'])
    assert_content_path('users', credentials['path'])

    for user in credentials['content']['users']:
        if not user.get('username'):
            raise exception.ProviException(
                "'{filepath}' malformed, users list contains "
                "an entry without 'username'. "
                "(see examples/demo_credentials.yaml)"
                .format(filepath=credentials['path']))
        if ':' in user['username']:
            raise exception.ProviException(
                "The password of user '{username}' cointains "
                "an invalid character ':'. Please fix '{filepath}'"
                .format(username=user['username'],
                        filepath=credentials['path']))
        if not user.get('password'):
            raise exception.ProviException(
                "'{filepath}' malformed, users list contains "
                "an entry without 'password'. "
                "(see examples/demo_credentials.yaml)"
                .format(filepath=credentials['path']))


def credential_name(credentials):
    """Get the basic file name for a credentials."""
    try:
        return os.path.splitext(os.path.basename(credentials['path']))[0]
    except Exception as e:
        raise exception.ProviException('Could not get name for credentials.'
                                       '(Supplied data structure might be'
                                       'invalid)') from e
